/**
 * There are 2 ways to interpolate variables in string
 * */
// 01- (Old school js)
var father : string = "fares kato"
console.log("Hello " + father + " Welcome to my website");


// 02- (Type script)
console.log(`Hello ${father} Welcome to my website`);
/**
* Classes Basics
* */
var SomeOne = (function () {
    // constructor
    function SomeOne(name, username) {
        this.username = username;
        this.age = 38;
        this.name = name;
    }
    SomeOne.prototype.printAge = function () {
        console.log(this.age);
    };
    SomeOne.prototype.setType = function (type) {
        this.type = type;
        console.log(this.type);
    };
    return SomeOne;
}());
var foo = new SomeOne('Fares', 'fares');
console.log(foo);
//person.printAge();
//person.setType('good guy'); 

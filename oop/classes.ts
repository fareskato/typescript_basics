/**
* Classes Basics
* */
class SomeOne{
    // attr
    name : string
    private type : string;
    protected age : number = 38;

    // constructor
    constructor(name : string, public username : string){
        this.name = name;
    }

    printAge(){
        console.log(this.age);
    }

    setType(type:string){
        this.type = type;
        console.log(this.type);
    }

}
let foo = new SomeOne('Fares', 'fares');
console.log(foo);
//person.printAge();
//person.setType('good guy');
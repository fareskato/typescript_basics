var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Person = (function () {
    // constructor
    function Person(name, username) {
        this.username = username;
        this.age = 27;
        this.name = name;
        //this.username = username;
    }
    Person.prototype.printAge = function () {
        console.log(this.age);
    };
    Person.prototype.setType = function (type) {
        this.type = type;
        console.log(this.type);
    };
    return Person;
}());
var somebody = new Person('Foo', 'foo');
//console.log(somebody);
// Inheritance
var Fares = (function (_super) {
    __extends(Fares, _super);
    function Fares(username) {
        var _this = _super.call(this, 'Fares', username) || this;
        _this.age = 38;
        return _this;
    }
    return Fares;
}(Person));
var me = new Fares('fares kato');
console.log(me);

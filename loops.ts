/**
* Loops
* */

let scores : number[] =  [100, 125, 150, 175];

/*
 * for in returns the element index in the collection
 * */
console.log('The result of for in');
for(let x in scores ){
    console.log(x);
}
console.log('The result of for of');

/*
 * for of returns the element value in the collection
 * */
for(let y of scores ){
    console.log(y);
}
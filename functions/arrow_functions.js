/**
 * Traditional function definition
 * */
function fullName(first, last) {
    return first + ' ' + last;
}
console.log(fullName('Fares', 'Kato')); // Fares Kato
/**
 * Arrow function definition
 * */
var otherFullName = function (first, last) {
    return first + ' ' + last;
};
console.log(otherFullName('Ashamaz', 'Kato')); // Ashamaz Kato

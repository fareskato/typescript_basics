/*
* Function arguments
* - To create optional argument put ? before :
* - The optional arguments should be the last arguments
* */
function getFullAddress(name : string, phone : number, city?: string, email = 'fares@fares.com') {
    if(city){
        console.log(name + phone + city + email  );
    }
    console.log(name + phone + email);
}
getFullAddress('fares', 232343);
getFullAddress('fares', 232343,'Moscow','test@test.com');
getFullAddress('fares', 232343,'Nalchek');

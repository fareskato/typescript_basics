/**
 * Traditional function definition
 * */

function fullName (first : string, last : string) : string {
    return first + ' ' + last;
}
console.log(fullName('Fares', 'Kato')); // Fares Kato

/**
 * Arrow function definition
 * */

var otherFullName = (first : string, last : string) : string => {
    return first + ' ' + last;
}

console.log(otherFullName('Ashamaz', 'Kato')); // Ashamaz Kato

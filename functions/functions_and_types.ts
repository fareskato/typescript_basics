/**
 *  Define the type of return value
 * */
let firstName : string = 'Fares';
function getFirstName () : string {
    return firstName;
}
console.log(getFirstName());

/**
 * Use avoid in case the function will not return anything
 * */
function doNothing() : void {
    console.log("do Nothing : don't return any thing!");
}
doNothing();

/**
 * Function arguments type
 * */
function addNumbers(val1:number, val2:number ) : number {
    return val1 + val2;
}
console.log(addNumbers(10, 25));
//console.log(addNumbers(10, '5'));  => error

/**
 * Functions as a type
 * */
function sayHi() {
    console.log('hello there!');
}

let myFunc;
myFunc = addNumbers;
console.log(myFunc(33, 22));
myFunc = sayHi;
myFunc();
console.log("******************* use function type *******************");
// but if we define the function type to take specific type
let myFuncType : (arg1: number, arg2: number) => number;
myFuncType = addNumbers;
console.log(myFuncType(55, 66));
//myFuncType = sayHi;
//console.log(myFuncType());  => error

function myMultiply(x : number, y: number) : number {
    return x * y;
}
// here I can use muFuncType (the args are numbers and so is the return value)
myFuncType = myMultiply;
console.log(myFuncType(100,25));

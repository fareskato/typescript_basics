// object definition                       = object
let myObj : {name: string, code: number} ={
    name: 'foofoo',
    code: 13242
}
myObj = {
    name: 'testtest',
    code: 78575
}

/*
 myObj = {
 name: 'testtest',
 code: '65373' // => thia should be number to match the object definition
 }
 */
console.log(myObj);
console.log('************************* create objects via type alias *******************************');
// create the type object alias
type Complex = {data: number[], output : (all:boolean) => number[] };
let firstComplexObject : Complex = {
    data: [1, 2, 3],
    output : function (all:boolean) : number[]{
        return this.data;
    }
}

let secondComplexObject : Complex = {
    data: [58, 62, 33],
    output : function (all:boolean) : number[]{
        return this.data;
    }
}

console.log(typeof firstComplexObject);
console.log(typeof secondComplexObject);
console.log(firstComplexObject);
console.log(secondComplexObject);


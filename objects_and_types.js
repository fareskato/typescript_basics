// object definition                       = object
var myObj = {
    name: 'foofoo',
    code: 13242
};
myObj = {
    name: 'testtest',
    code: 78575
};
/*
 myObj = {
 name: 'testtest',
 code: '65373' // => thia should be number to match the object definition
 }
 */
console.log(myObj);
console.log('************************* create objects via type alias *******************************');
var firstComplexObject = {
    data: [1, 2, 3],
    output: function (all) {
        return this.data;
    }
};
var secondComplexObject = {
    data: [58, 62, 33],
    output: function (all) {
        return this.data;
    }
};
console.log(typeof firstComplexObject);
console.log(typeof secondComplexObject);
console.log(firstComplexObject);
console.log(secondComplexObject);

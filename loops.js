/**
* Loops
* */
var scores = [100, 125, 150, 175];
/*
 * for in returns the element index in the collection
 * */
console.log('The result of for in');
for (var x in scores) {
    console.log(x);
}
console.log('The result of for of');
/*
 * for of returns the element value in the collection
 * */
for (var _i = 0, scores_1 = scores; _i < scores_1.length; _i++) {
    var y = scores_1[_i];
    console.log(y);
}
